import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///Used to Filter Clothing Items
bool isSelected1 = true;
bool isSelected2 = true;
bool isSelected3 = true;
bool isSelected4 = true;

///Used to Filter what is clicked on
bool top1 = false;
bool top2 = false;
bool top3 = false;
bool dress1 = false;
bool dress2 = false;
bool dress3 = false;
bool bottom1 = false;
bool bottom2 = false;
bool bottom3 = false;
bool shoe1 = false;
bool shoe2 = false;
bool shoe3 = false;

///Used to Filter if Shopping Cart or Purchase History is clicked on
bool isShoppingCart = false;
bool isHistory = false;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 3',
      home: HomePage(),
    );
  }
}

///Navigation Tab
///-----------------------------------------------------------------------------
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _pageController = PageController();
  List<Widget> _screens = [ Shopping(), Account() ];
  int _selectedIndex = 0;

  void _onPageChanged(int index) { setState(() {_selectedIndex = index;});}

  void _onitemTapped(int selectedIndex) {
    _pageController.jumpToPage(selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: _screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),

      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blueGrey[500],
        onTap: _onitemTapped,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: _selectedIndex == 0 ? Colors.white : Colors.grey[500],),
            title: Text('Explore', style: TextStyle(color: _selectedIndex == 0 ? Colors.white : Colors.grey[500])),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book, color: _selectedIndex == 1 ? Colors.white : Colors.grey[500]),
            title: Text('Items & Purchases', style: TextStyle(color: _selectedIndex == 1 ? Colors.white : Colors.grey[500])),
          ),
        ],
      ),
    );
  }
}

///Explore Page
///-----------------------------------------------------------------------------
class Shopping extends StatefulWidget {
  @override
  _ShoppingState createState() => _ShoppingState();
}

class _ShoppingState extends State<Shopping> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Explore'),
          backgroundColor: Colors.blueGrey[500],
        ),
        backgroundColor: Colors.grey[300],
        body: ListView(
          children: <Widget>[
            ///Filter Chips
            ///----------------------------------------------------------------------------------------------------------------------------------
            Wrap(
              children: [
                SizedBox(
                  width: 10,
                ),
                FilterChip(
                  label: Text('Tops'),
                  labelStyle: TextStyle(color: isSelected1 ? Colors.black : Colors.black),
                  selected: isSelected1,
                  onSelected: (bool selected) {
                    setState(() {
                      isSelected1 = !isSelected1;
                    });
                  },
                  selectedColor: Colors.orangeAccent[200],
                  checkmarkColor: Colors.black,
                ),
                FilterChip(
                  label: Text('Dresses'),
                  labelStyle: TextStyle(color: isSelected2 ? Colors.black : Colors.black),
                  selected: isSelected2,
                  onSelected: (bool selected) {
                    setState(() {
                      isSelected2 = !isSelected2;
                    });
                  },
                  selectedColor: Colors.orangeAccent[200],
                  checkmarkColor: Colors.black,
                ),
                FilterChip(
                  label: Text('Bottoms'),
                  labelStyle: TextStyle(color: isSelected3 ? Colors.black : Colors.black),
                  selected: isSelected3,
                  onSelected: (bool selected) {
                    setState(() {
                      isSelected3 = !isSelected3;
                    });
                  },
                  selectedColor: Colors.orangeAccent[200],
                  checkmarkColor: Colors.black,
                ),
                FilterChip(
                  label: Text('Shoes'),
                  labelStyle: TextStyle(color: isSelected1 ? Colors.black : Colors.black),
                  selected: isSelected4,
                  onSelected: (bool selected) {
                    setState(() {
                      isSelected4 = !isSelected4;
                    });
                  },
                  selectedColor: Colors.orangeAccent[200],
                  checkmarkColor: Colors.black,
                ),
              ],
            ),

            ///Tops
            ///----------------------------------------------------------------------------------------------------------------------------------
            Visibility(
              visible: isSelected1,
              child: Card(
                shadowColor: Colors.red[200],
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/blackt.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Black T', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: top1 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  top1 = !top1;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected1,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/sweater.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Sweater', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: top2 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  top2 = !top2;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected1,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/bluecollar.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Blue Collar', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: top3 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  top3 = !top3;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),

            ///Dresses
            ///----------------------------------------------------------------------------------------------------------------------------------
            Visibility(
              visible: isSelected2,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/reddress.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Red Dress', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: dress1 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  dress1 = !dress1;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected2,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/orangedress.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Orange Dress', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: dress2 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  dress2 = !dress2;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected2,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/tandress.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Tan Dress', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: dress3 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  dress3 = !dress3;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),

            ///Bottoms
            ///----------------------------------------------------------------------------------------------------------------------------------
            Visibility(
              visible: isSelected3,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/bluejeans.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Blue Jeans', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: bottom1 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  bottom1 = !bottom1;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected3,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/khakis.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Khakis', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: bottom2 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  bottom2 = !bottom2;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected3,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/uglyshorts.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Ugly Shorts', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: bottom3 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  bottom3= !bottom3;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),

            ///Shoes
            ///----------------------------------------------------------------------------------------------------------------------------------
            Visibility(
              visible: isSelected4,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/freshnikes.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Fresh Nikes', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: shoe1 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  shoe1 = !shoe1;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected4,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/blackheels.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Black Heels', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: shoe2 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  shoe2 = !shoe2;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: isSelected4,
              child: Card(
                shadowColor: Colors.red[200],
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ///---------------------------------------
                        Image.asset('images/lowquaters.png', fit: BoxFit.cover, width: 80),
                        Column(
                          children: <Widget>[
                            Text('Low Quarters', style: TextStyle(fontSize: 18),),
                            RaisedButton(
                              child: shoe3 ? Icon(Icons.remove) : Icon(Icons.add),
                              color: Colors.blueGrey[200],
                              onPressed: (){
                                setState(() {
                                  shoe3 = !shoe3;
                                });
                              },
                            )
                          ],
                        )
                        ///----------------------------------------
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        )
    );
  }
}

///Items & Purchases Page
///-----------------------------------------------------------------------------
class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  bool checkforItems() {
    bool result = false;

    if( top1 == true || top2 == true || top3 == true ||  shoe1 == true || shoe2 == true || shoe3 == true ||  dress1 == true || dress2 == true || dress3 == true ||  bottom1 == true || bottom2 == true || bottom3 == true  )
      result = true;

    return result;
  }

  void _showBottomSheetCallback(){
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
          decoration: BoxDecoration(
            border: Border(top: BorderSide(color: Colors.black)),
            color: Colors.orangeAccent[200],
          ),
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: checkforItems() ? Text('Your Items Were Successfully Purchased!', textAlign: TextAlign.center, style: TextStyle(fontSize: 24.0) )
              : Text('There Were No Items To Purchase', textAlign: TextAlign.center, style: TextStyle(fontSize: 24.0),
            ),
          )
      );
    });
  }

  ///Removes all items from shopping cart checklist
  void removeAllItems() {
    top1 = false;
    top2 = false;
    top3 = false;
    dress1 = false;
    dress2 = false;
    dress3 = false;
    bottom1 = false;
    bottom2 = false;
    bottom3 = false;
    shoe1 = false;
    shoe2 = false;
    shoe3 = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Items & Purchases'),
        backgroundColor: Colors.blueGrey[500],
      ),
      backgroundColor: Colors.grey[300],
      body: ListView(
        children: [
          ///Displays the two buttons in a Row
          ///-------------------------------------------------------------------
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RaisedButton(
                padding: const EdgeInsets.all(8.0),
                textColor: Colors.black,
                color: Colors.orangeAccent[200],
                onPressed: () {setState(() {
                  isShoppingCart = !isShoppingCart;
                });},
                child: new Text("Shopping Cart"),
              ),

              RaisedButton(
                padding: const EdgeInsets.all(8.0),
                textColor: Colors.black,
                color: Colors.orangeAccent[200],
                onPressed: _showBottomSheetCallback,
                child: new Text("Purchase Items"),
              ),

              RaisedButton(
                padding: const EdgeInsets.all(8.0),
                textColor: Colors.black,
                color: Colors.orangeAccent[200],
                onPressed: () {
                  removeAllItems();
                  showDialog(
                    context: context,
                    builder: (_) => cupertino(),);
                },
                child: new Text("Remove All"),
              ),
            ],
          ),

          ///Display Shopping Cart Data Table
          ///-------------------------------------------------------------------
          Visibility(
            visible: isShoppingCart,
            child: DataTable(
              columns: <DataColumn>[
                DataColumn(label: Text('Items', style: TextStyle(fontStyle: FontStyle.italic),),),
                DataColumn(label: Text('Price', style: TextStyle(fontStyle: FontStyle.italic),),),
                DataColumn(label: Text('Added', style: TextStyle(fontStyle: FontStyle.italic),),),
              ],
              rows: <DataRow>[
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Black T')),
                    DataCell(Text('\$25')),
                    top1 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Sweater'), ),
                    DataCell(Text('\$40')),
                    top2 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Blue Collar'), ),
                    DataCell(Text('\$90')),
                    top3 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Red Dress')),
                    DataCell(Text('\$50')),
                    dress1 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Orange Dress')),
                    DataCell(Text('\$50')),
                    dress2 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Tan Dress'), ),
                    DataCell(Text('\$100')),
                    dress3 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Blue Jeans')),
                    DataCell(Text('\$50')),
                    bottom1 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Khakis')),
                    DataCell(Text('\$50')),
                    bottom2 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Ugly Shorts'), ),
                    DataCell(Text('\$40')),
                    bottom3 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Fresh Nikes')),
                    DataCell(Text('\$1000')),
                    shoe1 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Black Heels')),
                    DataCell(Text('\$90')),
                    shoe2 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
                DataRow(
                  cells: <DataCell>[
                    DataCell(Text('Low Quarters'), ),
                    DataCell(Text('\$100')),
                    shoe3 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

///Cupertino Dialog
///-----------------------------------------------------------------------------
class cupertino extends StatefulWidget {
  @override
  _cupertinoState createState() => _cupertinoState();
}

class _cupertinoState extends State<cupertino> {
  @override

  void _openHome() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Items & Purchases'),
        backgroundColor: Colors.blueGrey[500],
      ),
      backgroundColor: Colors.grey[300],
      body: CupertinoAlertDialog(
        title: Text('Removed', style: TextStyle(fontSize: 20)),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Your Shopping Cart Is Empty', style: TextStyle(fontSize: 16)),
              SizedBox(height: 10,),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Return to Explore Page', style: TextStyle(fontSize: 16)),
            onPressed: () { _openHome();},
          )
        ],
      ),
    );
  }
}